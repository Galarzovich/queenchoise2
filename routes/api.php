<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/




Route::post('registro', 'App\Http\Controllers\VotosController@storeRegistro');

Route::post('auth/login', 'App\Http\Controllers\AuthController@login');
Route::post('auth/signup', 'App\Http\Controllers\AuthController@signup');
Route::get('auth/logout', 'AuthController@logout');
Route::get('usuarios', 'App\Http\Controllers\VotosController@getUsers');
Route::prefix('registros/{hashQr}')->group(function () {
    Route::get('',"App\Http\Controllers\VotosController@getOneUsers");
});
Route::group(['middleware' => ['auth:api']], function(){
Route::group(['prefix' => 'users' ], function(){
    Route::get("me","App\Http\Controllers\UsersController@currentUser");
});
    Route::post('votar', 'App\Http\Controllers\VotosController@store');
});
