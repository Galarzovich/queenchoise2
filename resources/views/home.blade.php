@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Hola, sacá captura de este codigo. Te servirá luego :)') }}</div>

                <div class="card-body" style="text-align: center">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                        <div>
                            <img src="{{asset('qrs/'.$hashQr.'.png')}}" height="500px" width="500px" >
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
