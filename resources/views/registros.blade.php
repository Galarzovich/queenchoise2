@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
               
                <div class="card-body" style="text-align: center">
                    <table class="table">
                        <thead>
                          <tr>
                            <th scope="col">#</th>
                            <th scope="col">DNI</th>
                            <th scope="col">Apellidos</th>
                            <th scope="col">Nombres</th>
                            <th scope="col">F.Nacimiento</th>
                          </tr>
                        </thead>
                        <tbody>
                            @foreach ($registros as $r)
                                <tr>
                                    <td>{{$r->idregistros}}</td>
                                    <td>{{$r->dni}}</td>
                                    <td>{{$r->apellidos}}</td>
                                    <td>{{$r->nombres}}</td>
                                    <td>{{$r->nacimiento}}</td>
                                </tr>
                           @endforeach
                        </tbody>
                      </table>
                
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
