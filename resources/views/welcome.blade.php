<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Fiesta Provincial de los Estudiantes 2022</title>
        <!-- Favicon-->
        <link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
        <!-- Font Awesome icons (free version)-->
        <script src="https://use.fontawesome.com/releases/v6.1.0/js/all.js" crossorigin="anonymous"></script>
        <!-- Simple line icons-->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.5.5/css/simple-line-icons.min.css" rel="stylesheet" />
        <!-- Google fonts-->
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="{{asset('assets/css/styles.css')}}" rel="stylesheet" />
    </head>
    <body id="page-top">
        <!-- Header-->
        <header class="masthead d-flex align-items-center">
            <div class="container px-4 px-lg-5 text-center">
                <h1 class="mb-title">Fiesta Provincial de los Estudiantes 2022</h1>
                <a class="btn btn-danger btn-xl" style="margin-top:50px; font-weight:bold" href="{{ route('register') }}">REGISTRARME</a>
            </div>
        </header>

           <!-- Portfolio-->
           <section class="content-section" id="portfolio">
            <div class="container px-4 px-lg-5">
               
                <div class="row gx-0">
                    <div class="col-lg-6">
                        <a class="portfolio-item" href="#!">
                            <div class="caption">
                            </div>
                            <img class="img-fluid" src="images/FB_IMG_1.jpg" alt="..." />
                        </a>
                    </div>
                    <div class="col-lg-6">
                        <a class="portfolio-item" href="#!">
                            <div class="caption">
                            </div>
                            <img class="img-fluid" src="/images/FB_IMG_5.jpg" alt="..." />
                        </a>
                    </div>
                    <div class="col-lg-6">
                        <a class="portfolio-item" href="#!">
                            <div class="caption">
                            </div>
                            <img class="img-fluid" src="/images/FB_IMG_3.jpg" alt="..." />
                        </a>
                    </div>
                    <div class="col-lg-6">
                        <a class="portfolio-item" href="#!">
                            <div class="caption">
                            </div>
                            <img class="img-fluid" src="/images/FB_IMG_4.jpg" alt="..." />
                        </a>
                    </div>
                </div>
            </div>
        </section>
        <!-- Services-->
        <section class="content-section bg-primary text-white text-center" id="services">
            <div class="container px-3 px-lg-5">
                <div class="row gx-3 gx-lg-4">
                    
                    <div class="col-lg-4 col-md-6 mb-5 mb-lg-0">
                        <span class="service-icon rounded-circle mx-auto mb-3"><img src="{{asset('images/logo3.png')}}" height="160px" width="150px"></i></span>
                        <p class="text-faded mb-0">Secretaria de Gobierno</p>
                    </div>

                    <div class="col-lg-4 col-md-6 mb-5 mb-lg-0">
                        <span class="service-icon rounded-circle mx-auto mb-3"><img style = "margin-top: 30px;" src="{{asset('images/logo1.png')}}" height="100px" width="100px"></i></span>
                        <p class="text-faded mb-0">Dirección de la Juventud</p>
                    </div>
                    
                    <div class="col-lg-4 col-md-6 mb-5 mb-lg-0">
                        <span class="service-icon rounded-circle mx-auto mb-3"><img style = "margin-top: 35px;" src="{{asset('images/logo4.png')}}" height="40px" width="150px"></i></span>
                        <p class="text-faded mb-0">Secretaria de Economia</p>
                    </div>


                </div>
            </div>
        </section>
       
        <!-- Footer-->
        <footer class="footer text-center">
            <div class="container px-4 px-lg-5">
                <ul class="list-inline mb-5">
                    <li class="list-inline-item">
                        <a class="social-link rounded-circle text-white mr-3" href="https://www.facebook.com/spciudad"><i class="icon-social-facebook"></i></a>
                    </li>
                </ul>
                <p class="text-muted small mb-0">Copyright &copy; Upperdata 2022</p>
            </div>
        </footer>

        <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top"><i class="fas fa-angle-up"></i></a>
        <!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Core theme JS-->
        <script src="{{asset('assets/js/scripts.js')}}"></script>
    </body>
</html>
