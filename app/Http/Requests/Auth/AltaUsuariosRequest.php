<?php

namespace App\Http\Requests\Auth;

use App\Enums\Medios;
use App\Enums\Ocupacion;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

class AltaUsuariosRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'     => 'required|string',
            'dni'     => 'required|string|unique:users',
            'password' => 'required|string|confirmed',
            'edad'     => 'required|string',
            'celular'     => 'required|string',
            'email'    => 'required|string|email|unique:users',
            'localidad'     => 'required|string',
            'ocupacion'     =>  'required|string',
            'conocerCandidata'     => 'required|string',
            'conocerEvento'     =>  'required|string'

        ];
    }

    public function messages()
    {
        return [
            'name.required'     => 'Por favor, ingrese un nombre.',
            'dni.required'  =>'Se esperaba un dni.',
            'email.required'    => 'Se esperaba un email',
            'email.unique'    => 'Ya existe una cuenta registrada con ese email.',
            'password.required' => 'Se esperaba un password',
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        throw new HttpResponseException(response()->json(['errors' => $errors
        ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY));
    }
}
