<?php

namespace App\Http\Controllers;

use App\Enums\TipoUsuario;
use App\Http\Requests\Auth\UpdateUserRequest;
use App\Models\Administradores;
use App\Models\Jefes;
use App\Models\User;
use App\Tools\ApiMessage;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class UsersController extends Controller
{
    /**
     * @OA\Get(
     *     path="/users",
     *     tags={"Usuarios"},
     *     summary="Obtener lista full de usuarios.",
     *     @OA\Response(
     *         response=200,
     *         description="Devuelve una lista de usuarios."
     *     )
     * )
     */
    public function index(Request $request)
    {
        $res = new ApiMessage($request);

        $all = User::all();
        $res->setData($all);

        return $res->send();
    }

    /**
     * @OA\Get(
     *     path="/users/lista-nombres",
     *     tags={"Usuarios"},
     *     summary="Mostrar una lista simple de todos los usuarios",
     *     @OA\Response(
     *         response=200,
     *         description="OK"
     *     )
     * )
     */
    public function listarNombres(Request $request )
    {
        $res = new ApiMessage($request);
        $lista = User::select(['id','nombre','tipo','dni'])->get();
        $res->setData($lista);
        return $res->send();
    }

    /**
     * @OA\Get(
     *     path="/users/me",
     *     tags={"Usuarios"},
     *     summary="Mostrar usuario actual",
     *     @OA\Response(
     *         response=200,
     *         description="Devuelve el usuario actual."
     *     )
     * )
     */
    public function currentUser(Request $request)
    {
        $res = new ApiMessage($request);
        $res->addLog("Obteniendo user..");

        /** @var User $user */
        $user = Auth::user();

        $userData = [];
        $userData['user'] = $user;

/*         switch (intval($user->tipo)){


            case TipoUsuario::JEFES:
                try{
                    // obtenemos el medico
                    $userData['jefes'] = Jefes::where('users_id',$user->id)
                        ->firstOrFail();
                }catch (Exception $e){
                    return $res->setCode(409)->setMessage("No se encontró jefe")->send();
                }
                break;
            case TipoUsuario::EMPLEADOS:
                try{
                    // obtenemos el medico
                    $userData['empleados'] = Administradores::where('users_id',$user->id)
                        ->firstOrFail();
                }catch (Exception $e){
                    return $res->setCode(409)->setMessage("No se encontró empleado")->send();
                }
                break;
        } */


        $res->setData($userData);

        return $res->send();
    }


    public function update(UpdateUserRequest $request, $id)
    {
        $res = new ApiMessage();
        $res->addLog("Actualizando el usaurio $id");

        $user = User::find($id);

        $camposValidos = $request->validated();
        // asignamos los campos modificados
        $user->fill($camposValidos);
        $user->save();

        return $res->send();
    }

    public function show(Request $request, $id)
    {
        $res = new ApiMessage($request);
        # buscamos el usuario
        $user = User::find($id);
        if(!$user){
            # el usuario no existe
            return $res->setCode(404)->setMessage("El usuario no existe.")->send();
        }
        # encontrado, devolvemos en $data
        $res->setData($user);

        return $res->send();
    }

    public function delete(Request $request,$id)
    {
        $res = new ApiMessage($request);
        $res->addLog("Eliminando el usuario $id");
        return $res->send();
    }


    public function obtenerAlgo(Request $request )
    {
        $res = new ApiMessage($request);
        $res->addLog("Obteniendo algo");

        return $res->send();
    }

}
