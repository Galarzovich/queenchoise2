<?php

namespace App\Http\Controllers;

use App\Http\Requests\CrearVotoRequest;
use App\Models\Registro;
use App\Models\User;
use App\Models\Voto;
use App\Tools\ApiMessage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class VotosController extends Controller
{


    public function store(CrearVotoRequest $request)
    {
        $respuesta = new ApiMessage();
        $datos = $request->validated();
        $user_id = auth('api')->user()->id;

        $datos['user_id'] = $user_id;
        $datos['reina_id'] = (int)$request->reina_id;

        if(!$user_id || !$datos)
        {
            return $respuesta->setCode(409)->setMessage("Ocurrio un error")->send();
        }

        $voto = Voto::create($datos);
        $respuesta->setMessage("Voto registrado con exito.")->setData($voto);
        return $respuesta->send();
    }


    public function storeRegistro(Request $request)
    {
        $respuesta = new ApiMessage();
     
        $datos['apellidos'] = $request->apellidos;
        $datos['nombres'] = $request->nombre;
        $datos['dni'] = $request->dni;
        $datos['nacimiento'] = $request->nacimiento;


        $registro = Registro::create($datos);
        $respuesta->setMessage("Registro creado con exito.")->setData($registro);
        return $respuesta->send();
    }

    public function getUsers(Request $request)
    {
        $respuesta = new ApiMessage($request);
        $lista = User::all();
        $respuesta->setData($lista);
        return $respuesta->send();
    }

    public function getOneUsers($hashQr)
    {
        $respuesta = new ApiMessage();
        $OneUser = User::where('hashQr',$hashQr)->get();
        $respuesta->setData($OneUser);
        return $respuesta->send();
    }


}
