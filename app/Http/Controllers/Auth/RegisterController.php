<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use LaravelQRCode\Facades\QRCode;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'dni' => ['required', 'integer', 'digits_between: 7,8'],
            'edad' => ['required', 'integer', 'digits_between: 1,3'],
            'celular' => ['required', 'integer', 'digits_between: 8,10'],
            'localidad' => ['required', 'string', 'max:255'],
            'ocupacion' => ['required', 'string', 'max:255'],
            'conocerCandidata' => ['required', 'string', 'max:255'],
            'conocerEvento' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {


        $datos = [
            'name' => $data['name'],
            'dni' => $data['dni'],
            'edad' => $data['edad'],
            'celular' => $data['celular'],
            'localidad' => $data['localidad'],
            'ocupacion' => $data['ocupacion'],
            'conocerCandidata' => $data['conocerCandidata'],
            'conocerEvento' => $data['conocerEvento'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'hashQr' => strtoupper(substr(md5(time()), 0, 15)),
        ];

        QRCode::text("https://estudiantes.saenzpena.gob.ar/api/registros/".$datos['hashQr'])->setSize(500)->setOutfile("qrs/".$datos['hashQr'].".png")->png();

        return User::create($datos);
    }
}
