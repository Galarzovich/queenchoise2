<?php

namespace App\Http\Controllers;
use App\Models\Registro;
use Illuminate\Http\Request;

class RegistrosController extends Controller
{
    public function index()
    {
        $registros = Registro::all();
    
        return view('registros')->with('registros',$registros);
    }

}
