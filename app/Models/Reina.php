<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reina extends Model
{
    protected $table = 'reinas';
    protected $fillable = [
        'name',
        'dni'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];
}
