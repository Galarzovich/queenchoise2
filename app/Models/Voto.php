<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Voto extends Model
{
    protected $table = 'votos';
    protected $fillable = [
        'user_id',
        'reina_id'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];
}
