<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Registro extends Model
{
    protected $table = 'registros';
    protected $fillable = [
        'apellidos',
        'nombres',
        'dni',
        'nacimiento'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];
}
